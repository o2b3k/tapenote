package ru.tapenote.tapenote;

import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by Bakhtiyorbek Begmatov on 9/11/2016.
 */

public final class ApiConstants {

    public static final String SERVER_URL = "http://tapenote.ru";

    public static final String NEWS_LIST_URL = "/api/v1/notes/list";
    public static final String VIEW_NEWS_URL = "/api/v1/notes/{id}";



    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 10000;
    public static final int SUCCESS_RESPONSE_CODE = 200;

    public static final String QUERY_PARAM_LANG = "lang";

    public static String getCompiledUrl(
            String url,
            @Nullable HashMap<String, Object> requiredParams,
            @Nullable HashMap<String, Object> optionalParams,
            @Nullable HashMap<String, Object> queryParams
    ) {
        String compiledUrl = SERVER_URL + url;
        if (requiredParams != null) {
            for (String key : requiredParams.keySet()) {
                compiledUrl = compiledUrl.replace(
                        "{" + key + "}",
                        String.valueOf(requiredParams.get(key))
                );
            }
        }

        if (optionalParams != null) {
            for (String key : optionalParams.keySet()) {
                compiledUrl = compiledUrl.replace(
                        "{" + key + "?}",
                        String.valueOf(optionalParams.get(key))
                );
            }
        }

        // Remove all optional parameters
        compiledUrl = compiledUrl.replaceAll("\\{[a-zA-Z_]+\\?\\}", "");

        // Adding query parameters
        if (queryParams != null) {
            char glue = !compiledUrl.contains("?") ? '?' : '&';
            StringBuilder queryString = new StringBuilder();

            for (String key : queryParams.keySet()) {
                if (queryString.length() != 0) {
                    queryString.append("&");
                }

                queryString.append(key).append('=').append(
                        String.valueOf(queryParams.get(key))
                );
            }

            compiledUrl = compiledUrl + glue + queryString.toString();
        }

        return compiledUrl;
    }

    public static OkHttpClient getHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.MILLISECONDS);

        return builder.build();
    }
}
