package ru.tapenote.tapenote.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import ru.tapenote.tapenote.R;
import ru.tapenote.tapenote.models.News;
import ru.tapenote.tapenote.network.GetNewsTask;
import ru.tapenote.tapenote.utils.PicassoImageGetter;
import com.squareup.picasso.Picasso;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class ViewNewsActivity extends LocaledBaseActivity {

    public static final String EXTRA_NEWS_ID = "news_id";

    private int newsId;
    private MaterialProgressBar loadingProgress;
    private LinearLayout newsLayout;
    private ImageView newsImage;
    private TextView newsTitle;
    private TextView newsContent;
    private LinearLayout connectionErrorLayout;

    private PicassoImageGetter imageGetter;
    private GetNewsTask getNewsTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_news);

        getSupportActionBar().setTitle(R.string.news);

        this.newsId = getIntent().getIntExtra(EXTRA_NEWS_ID, -1);
        if (this.newsId == -1) {
            finish();
        }

        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int innerWidth = displayMetrics.widthPixels - 2 * getResources().getDimensionPixelSize(
                R.dimen.activity_horizontal_margin
        );

        this.imageGetter = new PicassoImageGetter(this, innerWidth);

        this.loadingProgress = (MaterialProgressBar) findViewById(R.id.loading_progress);
        this.newsLayout = (LinearLayout) findViewById(R.id.news_layout);
        this.newsImage = (ImageView) findViewById(R.id.news_image);
        this.newsTitle = (TextView) findViewById(R.id.news_title);
        this.newsContent = (TextView) findViewById(R.id.news_content);
        this.connectionErrorLayout = (LinearLayout) findViewById(R.id.connection_error_layout);

        Button tryAgainButton = (Button) findViewById(R.id.try_again_button);
        tryAgainButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                loadData();
            }
        });

        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadData() {
        loadingProgress.setVisibility(View.VISIBLE);
        newsLayout.setVisibility(View.GONE);
        connectionErrorLayout.setVisibility(View.GONE);

        this.getNewsTask = new GetNewsTask(this, this.newsId) {

            @Override
            protected void onSuccess(final News news) {

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        newsLayout.setVisibility(View.VISIBLE);
                        loadingProgress.setVisibility(View.GONE);
                        connectionErrorLayout.setVisibility(View.GONE);
                        
                        if (news.getImageUrl() != null || news.getImageUrl().trim().length() > 0) {
                            Picasso.with(getContext()).load(news.getImageUrl()).into(newsImage);
                        } else {
                            newsImage.setVisibility(View.GONE);
                        }
                        newsTitle.setText(news.getTitle());
                        newsContent.setText(Html.fromHtml(news.getContent(), imageGetter, null));
                        newsContent.setMovementMethod(LinkMovementMethod.getInstance());
                    }
                });
            }

            @Override
            protected void onException(Exception e) {

                e.printStackTrace();

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        newsLayout.setVisibility(View.GONE);
                        loadingProgress.setVisibility(View.GONE);
                        connectionErrorLayout.setVisibility(View.VISIBLE);
                    }
                });

            }
        };

        this.getNewsTask.execute();
    }

    @Override
    protected void onDestroy() {
        if (this.getNewsTask != null) {
            this.getNewsTask.cancel();
        }

        super.onDestroy();
    }
}
