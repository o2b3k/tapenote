package ru.tapenote.tapenote.activities;

import android.content.Intent;
import android.os.Bundle;

import ru.tapenote.tapenote.R;

/**
 * Created by Bakhtiyorbek Begmatov on 15/10/2016.
 */

public class LauncherActivity extends LocaledBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_launcher);
    }

    @Override
    protected void onStart() {
        super.onStart();

        new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }

        }).start();
    }
}
