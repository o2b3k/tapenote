package ru.tapenote.tapenote.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import ru.tapenote.tapenote.R;
import ru.tapenote.tapenote.models.GeoPoint;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback {

    public static final String EXTRA_LATITUDE = "latitude";
    public static final String EXTRA_LONGITUDE = "longitude";
    public static final String EXTRA_PLACE_NAME = "place_name";

    private GeoPoint geoPoint;
    private String placeName;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        double latitude = getIntent().getDoubleExtra(EXTRA_LATITUDE, 0);
        double longitude = getIntent().getDoubleExtra(EXTRA_LONGITUDE, 0);

        if (latitude == 0 || longitude == 0) {
            finish();
            return;
        }

        this.geoPoint = new GeoPoint(latitude, longitude);
        this.placeName = getIntent().getStringExtra(EXTRA_PLACE_NAME);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng placePosition = new LatLng(this.geoPoint.getLatitude(), this.geoPoint.getLongitude());
        mMap.addMarker(new MarkerOptions().position(placePosition).title(this.placeName));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(placePosition, 15f));
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.setBuildingsEnabled(true);
    }
}
