package ru.tapenote.tapenote.activities;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;

import ru.tapenote.tapenote.utils.LocaledContextWrapper;

/**
 * Created by Bakhtiyorbek Begmatov on 07.12.2016.
 */

public class LocaledBaseActivity extends AppCompatActivity {

    /**
     * Wraps context to LocaledContextWrapper
     * @param newBase base context
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(LocaledContextWrapper.wrap(newBase));
    }
}
