package ru.tapenote.tapenote.activities;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import ru.tapenote.tapenote.AppPreferences;
import ru.tapenote.tapenote.Constants;
import ru.tapenote.tapenote.R;
import ru.tapenote.tapenote.fragments.NewsFragment;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends LocaledBaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String SELECTED_MENU_KEY = "selected_menu";

    private AppPreferences appPrefs;
    private int selectedMenuId = R.id.nav_news;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_draw_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().findItem(R.id.nav_news).setChecked(true);
        this.appPrefs = AppPreferences.getInstance(this);

        if (savedInstanceState == null) {
            selectFragmentByNavId(this.selectedMenuId);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (this.selectedMenuId == id) return true;

        switch (id) {

            case R.id.nav_language:
                new MaterialDialog.Builder(this)
                        .title(R.string.choose_language)
                        .items(R.array.languages)
                        .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {

                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                setLanguage(which);
                                return true;
                            }
                        })
                        .positiveText(R.string.choose)
                        .show();
                break;

            case R.id.nav_about:
                InputStream inputStream = getResources().openRawResource(
                        appPrefs.getLang().equals(Constants.LANG_UZ) ?
                                R.raw.about_app_uz : R.raw.about_app_ru
                );

                InputStreamReader reader = new InputStreamReader(inputStream);
                StringBuilder stringBuilder = new StringBuilder();
                char[] buffer = new char[1024 * 10];
                int length;

                try {
                    while ((length = reader.read(buffer)) != -1) {
                        stringBuilder.append(buffer, 0, length);
                    }
                } catch (IOException e) {
                    return false;
                }

                MaterialDialog dialog = new MaterialDialog.Builder(this)
                        .title(R.string.about_app)
                        .customView(R.layout.about_app_view, false)
                        .positiveText(R.string.close)
                        .build();

                TextView aboutAppText = (TextView) dialog.findViewById(android.R.id.text1);
                aboutAppText.setText(Html.fromHtml(stringBuilder.toString()));
                aboutAppText.setMovementMethod(new LinkMovementMethod());

                dialog.show();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void showFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void setLanguage(int langIndex) {
        switch (langIndex) {
            case 0:
                this.appPrefs.setLang(Constants.LANG_UZ);
                break;
            case 1:
                this.appPrefs.setLang(Constants.LANG_RU);
                break;
        }

        recreate();
    }

    private void selectFragmentByNavId(@IdRes int selectedMenuId) {
        Fragment fragment;
        switch (selectedMenuId) {
            case R.id.nav_news:
                fragment = new NewsFragment().instance();
                break;

            default:
                fragment = new NewsFragment().instance();
                break;
        }

        showFragment(fragment);
    }
}
