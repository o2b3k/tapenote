package ru.tapenote.tapenote;

import android.content.Context;


/**
 * Created by Bakhtiyorbek Begmatov on 10/11/2016.
 */

public final class Constants {

    public static final String LANG_UZ = "uz";
    public static final String LANG_RU = "ru";
    public static final String LANG_DEFAULT = LANG_RU;

    public static final int ROWS_PER_PAGE = 20;

}
