package ru.tapenote.tapenote.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.os.Build;

import ru.tapenote.tapenote.AppPreferences;
import ru.tapenote.tapenote.Constants;

import java.util.Locale;

/**
 * Created by Bakhtiyorbek Begmatov on 07.12.2016.
 */

public class LocaledContextWrapper extends ContextWrapper {

    public LocaledContextWrapper(Context base) {
        super(base);
    }

    public static ContextWrapper wrap(Context context) {
        AppPreferences appPrefs = AppPreferences.getInstance(context);
        Locale locale = new Locale(appPrefs.getLang(), appPrefs.getLang());
        Configuration configuration = context.getResources().getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(locale);
            context = context.createConfigurationContext(configuration);
        } else {
            configuration.locale = locale;
            context.getResources().updateConfiguration(
                    configuration,
                    context.getResources().getDisplayMetrics()
            );
        }

        return new LocaledContextWrapper(context);
    }
}
