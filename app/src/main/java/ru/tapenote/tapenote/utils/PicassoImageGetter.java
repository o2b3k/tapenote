package ru.tapenote.tapenote.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.Html;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by Win7 on 14.11.2016.
 */

public class PicassoImageGetter implements Html.ImageGetter {

    private Context context;
    private Drawable outDrawable;
    private int screenWidth;

    public PicassoImageGetter(Context context, int screenWidth) {
        this.context = context;
        this.screenWidth = screenWidth;
    }

    public Context getContext() {
        return context;
    }

    public int getScreenWidth() {
        return screenWidth;
    }

    @Override
    public Drawable getDrawable(String s) {

        Picasso.with(getContext())
                .load(s)
                .into(new Target() {

                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        float ratio = ((float) screenWidth) / bitmap.getWidth();

                        bitmap = Bitmap.createScaledBitmap(
                                bitmap,
                                screenWidth,
                                (int) (bitmap.getHeight() * ratio),
                                false
                        );

                        Drawable drawable = new BitmapDrawable(
                                getContext().getResources(),
                                bitmap
                        );

                        drawable.setBounds(
                                0,
                                0,
                                drawable.getIntrinsicWidth(),
                                drawable.getIntrinsicHeight()
                        );

                        setOutDrawable(drawable);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });

        return outDrawable;
    }

    public void setOutDrawable(Drawable outDrawable) {
        this.outDrawable = outDrawable;
    }
}
