package ru.tapenote.tapenote.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by Bakhtiyorbek Begmatov on 8/11/2016.
 */

public class News {

    private int id;
    private String lang;
    private String title;
    private String description;
    private String imageUrl;
    private String content;
    private int views;
    private Date date;

    public News(JSONObject json) throws JSONException {
        this.id = json.getInt("id");
        this.title = json.getString("title");
        this.description = json.getString("description");
        this.imageUrl = json.has("image") ? json.getString("image") : null;
        this.views = json.getInt("views");
        this.date = new Date(json.getInt("created") * 1000L);

        if (json.has("content")) {
            this.content = json.getString("content");
        }
    }

    public News(String jsonString) throws JSONException {
        this(new JSONObject(jsonString));
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getContent() {
        return content;
    }

    public int getViews() {
        return views;
    }

    public Date getDate() {
        return date;
    }
}
