package ru.tapenote.tapenote;

import android.content.Context;
import android.content.SharedPreferences;

import ru.tapenote.tapenote.Constants;

/**
 * Created by Bakhtiyorbek Begmatov on 10/11/2016.
 */

public class AppPreferences {

    // Preferences file name
    private static final String PREFERENCES_NAME = "App_preferences";

    // Preferences key names
    private static final String LANGUAGE_KEY = "lang";
    private static final String REGION_KEY = "region";

    // Single instance
    private static AppPreferences instance;

    private SharedPreferences preferences;

    private AppPreferences(Context context) {
        this.preferences = context.getSharedPreferences(
                PREFERENCES_NAME,
                Context.MODE_PRIVATE
        );
    }

    public static AppPreferences getInstance(Context context) {
        if (instance == null) {
            instance = new AppPreferences(context);
        }

        return instance;
    }

    public String getLang() {
        return this.preferences.getString(
                LANGUAGE_KEY,
                Constants.LANG_DEFAULT
        );
    }

    public void setLang(String lang) {
        this.preferences.edit()
                .putString(LANGUAGE_KEY, lang)
                .apply();
    }


    public SharedPreferences.Editor edit() {
        return this.preferences.edit();
    }
}
