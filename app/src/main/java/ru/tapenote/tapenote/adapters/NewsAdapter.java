package ru.tapenote.tapenote.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ru.tapenote.tapenote.R;
import ru.tapenote.tapenote.models.News;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bakhtiyorbek Begmatov on 13.11.2016.
 */

public class NewsAdapter extends ListAdapter<News> {

    public NewsAdapter(Context context, List<News> items) {
        super(context, items);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (loadingShown && position == this.items.size()) {
            return getLoadingView(position, convertView, parent);
        }

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item_cards, parent, false
            );
        }

        News news = getItem(position);

        if (news != null) {
            ImageView newsImage = (ImageView) convertView.findViewById(R.id.item_image);
            TextView newsTitle = (TextView) convertView.findViewById(R.id.item_title);
            TextView newsDescription = (TextView) convertView.findViewById(R.id.item_description);
            TextView newsViewed = (TextView) convertView.findViewById(R.id.item_viewed);
            TextView newsDate = (TextView) convertView.findViewById(R.id.item_date);

            if (news.getImageUrl() != null || news.getImageUrl().trim().length() > 0) {
                newsImage.setVisibility(View.VISIBLE);
                Picasso.with(getContext()).load(news.getImageUrl()).into(newsImage);
            } else {
                newsImage.setVisibility(View.GONE);
            }

            newsTitle.setText(news.getTitle());
            newsDescription.setText(news.getDescription());
            newsViewed.setText(getContext().getString(R.string.views_count, news.getViews()));

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            newsDate.setText(getContext().getString(
                    R.string.date_format,
                    dateFormat.format(news.getDate())
            ));
        }

        return convertView;
    }
}