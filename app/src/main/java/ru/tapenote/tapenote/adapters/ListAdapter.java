package ru.tapenote.tapenote.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import ru.tapenote.tapenote.R;
import ru.tapenote.tapenote.models.News;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static java.security.AccessController.getContext;

/**
 * Created by Win7 on 12.11.2016.
 */

public abstract class ListAdapter<T> extends BaseAdapter {

    private Context context;
    protected ArrayList<T> items;
    protected boolean loadingShown;

    public ListAdapter(Context context, List<T> items) {
        this.context = context;
        this.items = new ArrayList<>();
        this.setItems(items);
    }

    public Context getContext() {
        return this.context;
    }

    public void setItems(List<T> items) {
        for (T item : items) {
            this.items.add(item);
        }
    }

    @Override
    public int getCount() {
        return this.items.size() + (loadingShown ? 1 : 0);
    }

    @Override
    public T getItem(int position) {
        return this.items.get(position);
    }

    public void addItem(T item) {
        this.items.add(item);
    }

    public void remove(T item) {
        this.items.remove(item);
    }

    public void remove(int index) {
        this.items.remove(index);
    }

    public T getLastItem() {
        if (this.items.size() > 0) {
            return this.items.get(getCount() - (isLoadingShown() ? 2 : 1));
        }

        return null;
    }

    @Override
    public long getItemId(int position) {
        return this.items.get(position).hashCode();
    }

    @NonNull
    @Override
    public abstract View getView(int position, View convertView, @NonNull ViewGroup parent);

    protected View getLoadingView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item_loading, parent, false
            );
        }

        convertView.setClickable(false);
        return convertView;
    }

    public boolean isLoadingShown() {
        return loadingShown;
    }

    public void setLoadingShown(boolean loadingShown) {
        this.loadingShown = loadingShown;
        notifyDataSetChanged();
    }
}
