package ru.tapenote.tapenote.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import ru.tapenote.tapenote.ApiConstants;
import ru.tapenote.tapenote.R;
import ru.tapenote.tapenote.activities.ViewNewsActivity;
import ru.tapenote.tapenote.adapters.ListAdapter;
import ru.tapenote.tapenote.adapters.NewsAdapter;
import ru.tapenote.tapenote.models.News;
import ru.tapenote.tapenote.network.GetNewsListTask;

import java.util.List;

import static ru.tapenote.tapenote.Constants.ROWS_PER_PAGE;

/**
 * Created by Bakhtiyorbek Begmatov on 21.11.2016.
 */

public class NewsFragment extends ListFragment<News> {

    private GetNewsListTask getNewsListTask;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle(R.string.news);
    }

    @Override
    public ListAdapter createAdapter(Context context, List<News> items) {
        return new NewsAdapter(getContext(), items);
    }

    @Override
    public void loadData() {

        if (isLoading) return;

        if (adapter == null) {
            showLoading();
        }

        this.getNewsListTask = new GetNewsListTask(getContext()) {

            @Override
            protected void onSuccess(final List<News> items) {

                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if (adapter == null) {
                            if (items.size() == 0) {
                                showNoDataLayout();
                            }
                            else if (list.getVisibility() != View.VISIBLE) {
                                showList();
                            }
                            adapter = createAdapter(getContext(), items);
                            list.setAdapter(adapter);
                        } else {
                            for (News news : items) {
                                adapter.addItem(news);
                            }

                            adapter.setLoadingShown(false);
                        }
                    }
                });

                shouldLoadData = items.size() == ROWS_PER_PAGE;
            }

            @Override
            protected void onException(Exception e) {
                if (adapter == null) {
                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            showConnectionError();
                        }
                    });
                } else {
                    Toast.makeText(getContext(), R.string.connection_error, Toast.LENGTH_LONG).show();
                    adapter.setLoadingShown(false);
                }
            }

            @Override
            protected void onDone(boolean isSuccess) {
                isLoading = false;
            }
        };

        if (shouldLoadData) {
            this.getNewsListTask.getQueryParams().put(ApiConstants.QUERY_PARAM_LANG, appPrefs.getLang());
            if (adapter != null) {
                News news = (News) adapter.getLastItem();
                this.getNewsListTask.getQueryParams().put(
                        "last_news_time",
                        news.getDate().getTime() / 1000
                );

                adapter.setLoadingShown(true);
            }

            isLoading = true;
            this.getNewsListTask.execute();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        News news = (News) adapterView.getItemAtPosition(position);
        Intent intent = new Intent(getContext(), ViewNewsActivity.class);
        intent.putExtra(ViewNewsActivity.EXTRA_NEWS_ID, news.getId());
        startActivity(intent);
    }

    @Override
    public void onPause() {
        super.onPause();

        if (this.getNewsListTask != null) {
            this.getNewsListTask.cancel();
        }
    }
}
