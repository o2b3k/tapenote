package ru.tapenote.tapenote.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

import ru.tapenote.tapenote.AppPreferences;
import ru.tapenote.tapenote.R;
import ru.tapenote.tapenote.activities.ViewNewsActivity;
import ru.tapenote.tapenote.adapters.ListAdapter;
import ru.tapenote.tapenote.models.News;

import java.util.List;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

/**
 * Created by Bakhtiyorbek Begmatov on 10/11/2016.
 */

public abstract class ListFragment<T> extends Fragment
        implements AdapterView.OnItemClickListener,
                   AbsListView.OnScrollListener {

    protected AppPreferences appPrefs;

    protected MaterialProgressBar loadingProgress;
    protected LinearLayout connectionErrorLayout;
    protected LinearLayout noDataLayout;

    protected ListView list;
    protected ListAdapter adapter;

    protected boolean shouldLoadData = true;
    protected boolean isLoading;

    public ListFragment instance() {
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState
    ) {
        this.appPrefs = AppPreferences.getInstance(getContext());
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.loadingProgress = (MaterialProgressBar) view.findViewById(R.id.loading_progress);
        this.connectionErrorLayout = (LinearLayout) view.findViewById(R.id.connection_error_layout);
        this.noDataLayout = (LinearLayout) view.findViewById(R.id.no_data_layout);

        Button tryAgainButton = (Button) view.findViewById(R.id.try_again_button);
        tryAgainButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                loadData();
            }

        });

        this.list = (ListView) view.findViewById(android.R.id.list);
        this.list.setOnItemClickListener(this);
        this.list.setOnScrollListener(this);

        loadData();
    }

    public abstract ListAdapter createAdapter(Context context, List<T> items);

    public abstract void loadData();

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {}

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {}

    @Override
    public void onScroll(
            AbsListView absListView,
            int firstVisibleItem,
            int visibleItemCount,
            int totalItemCount
    ) {
        if (shouldLoadData) {
            if (totalItemCount != 0 && firstVisibleItem + visibleItemCount == totalItemCount) {
                loadData();
            }
        }
    }

    public void showLoading() {
        this.loadingProgress.setVisibility(View.VISIBLE);
        this.connectionErrorLayout.setVisibility(View.GONE);
        this.list.setVisibility(View.GONE);
        this.noDataLayout.setVisibility(View.GONE);
    }

    public void showConnectionError() {
        this.loadingProgress.setVisibility(View.GONE);
        this.connectionErrorLayout.setVisibility(View.VISIBLE);
        this.list.setVisibility(View.GONE);
        this.noDataLayout.setVisibility(View.GONE);
    }

    public void showList() {
        this.loadingProgress.setVisibility(View.GONE);
        this.connectionErrorLayout.setVisibility(View.GONE);
        this.list.setVisibility(View.VISIBLE);
        this.noDataLayout.setVisibility(View.GONE);
    }

    public void showNoDataLayout() {
        this.connectionErrorLayout.setVisibility(View.GONE);
        this.list.setVisibility(View.GONE);
        this.loadingProgress.setVisibility(View.GONE);
        this.noDataLayout.setVisibility(View.VISIBLE);
    }
}
