package ru.tapenote.tapenote.network.Exceptions;

/**
 * Created by Bakhtiyorbek Begmatov on 10/11/2016.
 */

public class BadResponseCodeException extends Exception {

    public BadResponseCodeException(String message) {
        super(message);
    }
}
