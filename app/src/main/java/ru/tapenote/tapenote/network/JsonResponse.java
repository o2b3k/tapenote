package ru.tapenote.tapenote.network;

import android.support.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.Reader;

import okhttp3.Response;

/**
 * Created by Bakhtiyorbek Begmatov on 10/11/2016.
 */

public class JsonResponse {

    public enum JsonRootType {
        TYPE_JSON_OBJECT,
        TYPE_JSON_ARRAY
    }

    private JSONObject jsonObject;
    private JSONArray jsonArray;

    private JsonRootType jsonRootType;

    public JsonResponse(Response response) throws IOException, JSONException {
        String jsonString = getResponseBody(response);

        JSONTokener jsonTokener = new JSONTokener(jsonString);
        Object root = jsonTokener.nextValue();

        if (root instanceof JSONObject) {
            this.jsonRootType = JsonRootType.TYPE_JSON_OBJECT;
            this.jsonObject = (JSONObject) root;
        }
        else {
            this.jsonRootType = JsonRootType.TYPE_JSON_ARRAY;
            this.jsonArray = (JSONArray) root;
        }
    }

    public JsonRootType getRootType() {
        return this.jsonRootType;
    }

    @NonNull
    private String getResponseBody(Response response) throws IOException {
        StringBuilder responseBody = new StringBuilder();
        char[] buffer = new char[1024 * 10];    // 10kb buffer
        int length;

        Reader reader = response.body().charStream();

        while ((length = reader.read(buffer)) != -1) {
            responseBody.append(buffer, 0, length);
        }

        return responseBody.toString();
    }

    public JSONObject getJsonObject() {
        return this.jsonObject;
    }

    public JSONArray getJsonArray() {
        return this.jsonArray;
    }
}
