package ru.tapenote.tapenote.network;

import android.content.Context;

import ru.tapenote.tapenote.ApiConstants;
import ru.tapenote.tapenote.models.News;
import ru.tapenote.tapenote.network.Exceptions.BadResponseCodeException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Win7 on 13.11.2016.
 */

public abstract class GetNewsTask extends BaseNetworkTask<News> {

    public GetNewsTask(Context context, int newsId) {
        super(context, ApiConstants.VIEW_NEWS_URL);

        this.requiredParams.put("id", newsId);
    }

    @Override
    public News parse(Response response) throws IOException, BadResponseCodeException, JSONException {
        JsonResponse jsonResponse = new JsonResponse(response);
        if (jsonResponse.getRootType() != JsonResponse.JsonRootType.TYPE_JSON_OBJECT) {
            throw new BadResponseCodeException("Invalid response type");
        }

        JSONObject json = jsonResponse.getJsonObject();
        if (!json.getString("status").equals("success")) {
            throw new BadResponseCodeException(json.getString("message"));
        }

        return new News(json.getJSONObject("data"));
    }
}
