package ru.tapenote.tapenote.network;

import android.content.Context;
import android.os.Looper;

import ru.tapenote.tapenote.ApiConstants;
import ru.tapenote.tapenote.network.Exceptions.BadResponseCodeException;

import org.json.JSONException;

import java.io.IOException;
import java.io.PipedReader;
import java.util.HashMap;

import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Bakhtiyorbek Begmatov on 10/11/2016.
 */

public abstract class BaseNetworkTask<T> {

    enum TaskState {
        TASK_STATE_IDLE,
        TASK_STATE_RUNNING,
        RASK_STATE_CANCELLED,
        TASK_STATE_DONE
    };

    private Thread thread;
    private Context context;

    protected String requestUrl;
    protected HashMap<String, Object> requiredParams;
    protected HashMap<String, Object> optionalParams;
    protected HashMap<String, Object> queryParams;

    private TaskState taskState;


    public BaseNetworkTask(Context context, String requestUrl) {
        this.context = context;
        this.requestUrl = requestUrl;

        this.requiredParams = new HashMap<>();
        this.optionalParams = new HashMap<>();
        this.queryParams = new HashMap<>();

        this.taskState = TaskState.TASK_STATE_IDLE;
    }

    protected Context getContext() {
        return this.context;
    }

    public void execute() {
        this.thread = new Thread(new Runnable() {

            @Override
            public void run() {
                Looper.prepare();
                taskState = TaskState.TASK_STATE_RUNNING;

                boolean isSuccessful = false;
                try {
                    T result = BaseNetworkTask.this.run();

                    if (taskState != TaskState.RASK_STATE_CANCELLED) {
                        onSuccess(result);
                    }

                    isSuccessful = true;
                }
                catch (IOException | JSONException | BadResponseCodeException e) {
                    if (taskState != TaskState.RASK_STATE_CANCELLED) {
                        onException(e);
                    }
                }

                onDone(isSuccessful);
                taskState = TaskState.TASK_STATE_DONE;
                Looper.loop();
            }
        });

        this.thread.start();
    }

    private T run() throws IOException, JSONException, BadResponseCodeException {
        Request request = this.makeRequest();
        Response response = ApiConstants.getHttpClient().newCall(request).execute();

        checkResponseCode(response.code());

        return parse(response);
    };

    protected Request makeRequest() {
        return new Request.Builder()
                .url(ApiConstants.getCompiledUrl(
                        this.getRequestUrl(),
                        this.getRequiredParams(),
                        this.getOptionalParams(),
                        this.getQueryParams()
                ))
                .get()
                .build();
    }

    protected void checkResponseCode(int responseCode) throws BadResponseCodeException {
        if (responseCode != ApiConstants.SUCCESS_RESPONSE_CODE) {
            throw new BadResponseCodeException("Invalid response code: " + responseCode);
        }
    }

    public abstract T parse(Response response) throws IOException, BadResponseCodeException, JSONException;

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public HashMap<String, Object> getRequiredParams() {
        return requiredParams;
    }

    public HashMap<String, Object> getOptionalParams() {
        return optionalParams;
    }

    public HashMap<String, Object> getQueryParams() {
        return queryParams;
    }

    protected abstract void onSuccess(T result);

    protected abstract void onException(Exception e);

    protected void onDone(boolean isSuccess) {}

    public boolean isRunning() {
        return taskState == TaskState.TASK_STATE_RUNNING;
    }

    public void cancel() {
        if (isRunning()) {
            this.taskState = TaskState.RASK_STATE_CANCELLED;
        }
    }
}
