package ru.tapenote.tapenote.network;

import android.content.Context;

import ru.tapenote.tapenote.ApiConstants;
import ru.tapenote.tapenote.models.News;
import ru.tapenote.tapenote.network.Exceptions.BadResponseCodeException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Bakhtiyorbek Begmatov on 10/11/2016.
 */

public abstract class GetNewsListTask extends BaseNetworkTask<List<News>> {

    public GetNewsListTask(Context context) {
        super(context, ApiConstants.NEWS_LIST_URL);
    }

    @Override
    public List<News> parse(Response response) throws IOException, BadResponseCodeException, JSONException {
        JsonResponse jsonResponse = new JsonResponse(response);
        if (jsonResponse.getRootType() != JsonResponse.JsonRootType.TYPE_JSON_OBJECT) {
            throw new BadResponseCodeException("Invalid response format");
        }

        ArrayList<News> list = new ArrayList<>();
        JSONObject json = jsonResponse.getJsonObject();
        if (!json.getString("status").equals("success")) {
            throw new BadResponseCodeException(json.getString("message"));
        }

        JSONArray newsArray = json.getJSONArray("data");
        int length = newsArray.length();

        for (int i=0; i<length; i++) {
            News news = new News(newsArray.getJSONObject(i));
            list.add(news);
        }

        return list;
    }
}
